<?php namespace App\Library {

	use \DateTimeZone;
	use \stdClass;
	use \Config;
  use \PushNotification;

  class NotificationUtils {

			public function send($type,$title,$message_text,$user_tokens,$issue_id = 0){

				$tokens = array();
				foreach ($user_tokens as $user_token) {
					if($user_token != ""){
							$tokens[] = PushNotification::Device($user_token);
					}
				}

				//check if tokens of users exists
				if(count($tokens) > 0){
					$devices = PushNotification::DeviceCollection($tokens);

					if($type == "general"){
						$click_action = 'com.tengofreestudio.sispe.MAIN_ACTIVITY';
						$custom = array(
									'type' => $type
								);
					}else if($type == "issue"){
						$click_action = 'com.tengofreestudio.sispe.MAIN_ACTIVITY';
						$custom = array(
									'type' => $type,
									'issue' => array(
										"id" => $issue_id != 0 ? $issue_id : 0
									)
								);
					}

					$message = PushNotification::Message($message_text,array(
							'badge' => 1,
							'title' => $title,
							'click_action' => $click_action,
							//'launchImage' => 'image.jpg',

							'custom' => $custom
					));

					$collection = PushNotification::app('appNameAndroid')->to($devices);
					$collection->adapter->setAdapterParameters(['sslverifypeer' => false]);
					$collection->send($message);

					foreach ($collection->pushManager as $push) {
							$response = $push->getAdapter()->getResponse();
					}

				} else {
					$response = "no hay usuarios disponibles";
				}

        return $response;
      }

  }

}
