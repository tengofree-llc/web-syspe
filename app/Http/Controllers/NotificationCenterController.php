<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Models\GcmToken;
use App\Models\JobOffer;
use App\Models\Course;
use App\Models\Meeting;

use \Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotificationCenterController extends Controller
{

    public function send(Request $request){

      $data = array();
      $this->validate($request, [
          'type' => 'required',
          'title' => 'required',
          'google_token' => 'required'
      ]);

      try{

          $notificationUtils = new \App\Library\NotificationUtils;
          $data["notification"] = $notificationUtils->send(
            $request->input('type'),
            $request->input('title'),
            $request->input('issue_subject'),
            $request->input('google_token'),
            $request->input('issue_id'));

          $data["status_data"] = array("status" => true, "message" => "Se ha enviado la notificación correctamente");

      }catch(Exception $e){
         $data["status_data"] = array("status" => false, "message" => $e->getMessage());
      }

      // foreach ($request->input('google_token') as $gogle_token) {
      //   $data[]= $gogle_token;
      // }
      // $data["type"] = $request->input('type');
      // $data["title"] = $request->input('title');
      // $data["issue_subject"] = $request->input('issue_subject');
      // $data["issue_id"] = $request->input('issue_id');

      return response()->json($data);
    }
}
